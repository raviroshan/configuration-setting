# README #

Configuration & Setting for - Environment Setup, Editor Config, ES-Lint config etc.

### What is this repository for? ###

* [.bash_profile - MacOS Env Variable](https://apple.stackexchange.com/questions/106778/how-do-i-set-environment-variables-on-os-x/229941#229941)
 How to set MacOS environment variable for JDK, Tomcat etc
* [.eslintrc - ESLint Config](https://eslint.org/)
The pluggable linting utility for JavaScript and JSX
* [.editorconfig - Code Editor Config](http://editorconfig.org/)
EditorConfig helps developers define and maintain consistent coding styles between different editors and IDEs.


### Who do I talk to? ###

* [Ravi Roshan](http://www.raviroshan.info/)